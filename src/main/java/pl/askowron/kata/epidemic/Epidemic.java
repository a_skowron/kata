package pl.askowron.kata.epidemic;

public class Epidemic {
    private final Population population;
    private final int daysToLiveWithInfection;
    private final long infectionSpreadRatio;
    private int day;
    private long dead;

    Epidemic(
            Population population,
            long numberOfInitiallyInfected,
            int daysToLiveWithInfection,
            long infectionSpreadRatio) {
        this.population = population;
        this.daysToLiveWithInfection = daysToLiveWithInfection;
        this.infectionSpreadRatio = infectionSpreadRatio;
        day = 1;
        dead = 0;
        population.infect(numberOfInitiallyInfected);
    }

    void passOneDay() {
        long newlyInfected = population.getNumberOfInfected() * infectionSpreadRatio;
        population.infect(newlyInfected);
        dead += population.dieOfInfection(daysToLiveWithInfection);
        day++;
    }

    @Override
    public String toString() {
        return "Epidemic " +
                "day=" + day +
                " -> population=" + population +
                ", dead=" + dead;
    }
}
