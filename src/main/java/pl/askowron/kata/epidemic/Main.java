package pl.askowron.kata.epidemic;

/*
* Epidemic kata
*
* On the first day, one person gets infected with a deadly virus.
* Each infected person infects one healthy person each day while she or he is alive.
* After three days, the infected person dies and stops spreading the disease.
* On which day will all people on Earth die?
*/

public class Main {

    private static final long TOTAL_EARTH_POPULATION = 5727000000L;
    private static final int DAYS_TO_LIVE_WITH_INFECTION = 3;
    private static final int NEW_INFECTIONS_CAUSED_BY_ONE_INFECTED_PERSON_PER_DAY = 1;
    private static final long NUMBER_OF_INITIALLY_INFECTED = 1L;

    public static void main(String[] args) {

        Population population = new Population(TOTAL_EARTH_POPULATION);
        Epidemic epidemic = new Epidemic(
                population,
                NUMBER_OF_INITIALLY_INFECTED,
                DAYS_TO_LIVE_WITH_INFECTION,
                NEW_INFECTIONS_CAUSED_BY_ONE_INFECTED_PERSON_PER_DAY);

        System.out.println(epidemic);
        while (population.getNumberOfAlive() > 0) {
            epidemic.passOneDay();
            System.out.println(epidemic);
        }
    }
}
