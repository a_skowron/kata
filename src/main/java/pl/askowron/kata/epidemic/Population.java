package pl.askowron.kata.epidemic;

import java.util.LinkedList;
import java.util.List;

public class Population {
    private long healthy;
    private List<Long> infectedEachDay;

    Population(long healthy) {
        this.healthy = healthy;
        this.infectedEachDay = new LinkedList<>();
    }

    long getNumberOfHealthy() {
        return healthy;
    }

    long getNumberOfInfected() {
        return infectedEachDay
                .stream()
                .reduce(Long::sum)
                .orElse(0L);
    }

    long getNumberOfAlive() {
        return getNumberOfHealthy() + getNumberOfInfected();
    }

    long infect(long toInfect) {
        if (toInfect > healthy) {
            return infect(healthy);
        } else {
            healthy -= toInfect;
            infectedEachDay.add(toInfect);
            return toInfect;
        }
    }

    long dieOfInfection(int daysFromInfectionToDeath) {
        long dead = 0;
        while (getNumberOfInfectionDaysStillAlive() > daysFromInfectionToDeath) {
            dead += infectedEachDay.remove(0);
        }
        return dead;
    }

    private int getNumberOfInfectionDaysStillAlive() {
        return infectedEachDay.size();
    }

    @Override
    public String toString() {
        return "{ " +
                "alive=" + getNumberOfAlive() +
                ", healthy=" + getNumberOfHealthy() +
                ", infected=" + getNumberOfInfected() +
                " }";
    }
}
