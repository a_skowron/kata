package pl.askowron.kata.epidemic;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EpidemicTest {

    @Test
    public void getEmptyPopulationOneDayLaterTest() {
        // Given
        Population population = new Population(0L);
        long numberOfInitiallyInfected = 1L;
        int daysToLiveWithInfection = 1;
        long newInfectionsCausedByOneInfectedPersonPerDay = 2L;
        Epidemic epidemic = new Epidemic(
                population,
                numberOfInitiallyInfected,
                daysToLiveWithInfection,
                newInfectionsCausedByOneInfectedPersonPerDay);
        // When
        epidemic.passOneDay();
        // Then
        assertEquals(0L, population.getNumberOfAlive());
        assertEquals(0L, population.getNumberOfHealthy());
        assertEquals(0L, population.getNumberOfInfected());
    }

    @Test
    public void getPopulationOneDayLaterTest() {
        // Given
        Population population = new Population(Long.MAX_VALUE);
        long numberOfInitiallyInfected = 1L;
        int daysToLiveWithInfection = 1;
        long newInfectionsCausedByOneInfectedPersonPerDay = 2L;
        Epidemic epidemic = new Epidemic(
                population,
                numberOfInitiallyInfected,
                daysToLiveWithInfection,
                newInfectionsCausedByOneInfectedPersonPerDay);
        // When
        epidemic.passOneDay();
        // Then
        assertEquals(Long.MAX_VALUE - 1L, population.getNumberOfAlive());
        assertEquals(Long.MAX_VALUE - 1L - 2L, population.getNumberOfHealthy());
        assertEquals(2L, population.getNumberOfInfected());
    }
}