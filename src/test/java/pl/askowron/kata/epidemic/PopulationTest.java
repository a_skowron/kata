package pl.askowron.kata.epidemic;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PopulationTest {
    @Test
    public void newEmptyPopulationTest() {
        // Given
        Population population = new Population(0L);
        // then
        assertEquals(0L, population.getNumberOfAlive());
        assertEquals(0L, population.getNumberOfHealthy());
        assertEquals(0L, population.getNumberOfInfected());
    }


    @Test
    public void newNonemptyPopulationTest() {
        // Given
        Population population = new Population(Long.MAX_VALUE);
        // then
        assertEquals(Long.MAX_VALUE, population.getNumberOfAlive());
        assertEquals(Long.MAX_VALUE, population.getNumberOfHealthy());
        assertEquals(0L, population.getNumberOfInfected());
    }

    @Test
    public void emptyPopulationDieTest() {
        // Given
        Population population = new Population(0L);
        // when
        long actuallyDied = population.dieOfInfection(0);
        actuallyDied += population.dieOfInfection(Integer.MAX_VALUE);
        // then
        assertEquals(0L, population.getNumberOfAlive());
        assertEquals(0L, population.getNumberOfHealthy());
        assertEquals(0L, population.getNumberOfInfected());
        assertEquals(0L, actuallyDied);
    }

    @Test
    public void infectPopulationOneTimeTest() {
        // Given
        Population population = new Population(123L);
        // when
        long actuallyInfected = population.infect(22L);
        // then
        assertEquals(123L, population.getNumberOfAlive());
        assertEquals(101L, population.getNumberOfHealthy());
        assertEquals(22L, population.getNumberOfInfected());
        assertEquals(22L, actuallyInfected);
    }

    @Test
    public void infectEntirePopulationTest() {
        // Given
        Population population = new Population(Long.MAX_VALUE);
        // when
        long actuallyInfected = population.infect(Long.MAX_VALUE);
        // then
        assertEquals(Long.MAX_VALUE, population.getNumberOfAlive());
        assertEquals(0L, population.getNumberOfHealthy());
        assertEquals(Long.MAX_VALUE, population.getNumberOfInfected());
        assertEquals(Long.MAX_VALUE, actuallyInfected);
    }

    @Test
    public void infectEmptyPopulationTest() {
        // Given
        Population population = new Population(0L);
        // when
        long actuallyInfected = population.infect(Long.MAX_VALUE);
        // then
        assertEquals(0L, population.getNumberOfAlive());
        assertEquals(0L, population.getNumberOfHealthy());
        assertEquals(0L, population.getNumberOfInfected());
        assertEquals(0L, actuallyInfected);
    }

    @Test
    public void infectMoreThanPopulationHasTest() {
        // Given
        Population population = new Population(123L);
        // when
        long actuallyInfected = population.infect(Long.MAX_VALUE);
        // then
        assertEquals(123L, population.getNumberOfAlive());
        assertEquals(0L, population.getNumberOfHealthy());
        assertEquals(123L, population.getNumberOfInfected());
        assertEquals(123L, actuallyInfected);
    }

    @Test
    public void infectAndDieSeveralTimesTest() {
        // Given
        Population population = new Population(123L);
        // when
        long actuallyInfected = population.infect(1L);
        actuallyInfected += population.infect(7L);
        actuallyInfected += population.infect(11L);
        actuallyInfected += population.infect(4L);
        long actuallyDied1 = population.dieOfInfection(4);
        long actuallyDied2 = population.dieOfInfection(2);
        long actuallyDied3 = population.dieOfInfection(2);
        actuallyInfected += population.infect(4L);
        // then
        assertEquals(115L, population.getNumberOfAlive());
        assertEquals(96L, population.getNumberOfHealthy());
        assertEquals(19L, population.getNumberOfInfected());
        assertEquals(27L, actuallyInfected);
        assertEquals(0L, actuallyDied1);
        assertEquals(8L, actuallyDied2);
        assertEquals(0L, actuallyDied3);
    }
}
